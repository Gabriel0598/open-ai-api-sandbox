# Open AI API Sandbox

***

## Name
OPEN AI API Sandbox

## Description
Repository of exploration and creation of projects with OpenAI API

## Badges
...

## Visuals
...

## Installation
Create a virtual environment in python, store your api key in pyvenv.cfg and call this config file to get request in API.

## Usage
Open to use, but it's necessary has credits in open ai api in order to use.

## Support
If you wanna contribute with something you can open a pull request.

## Roadmap
In a first moment I'm exploring platform.

## Contributing
...

## Authors and acknowledgment
Gabriel A. Ferreira

## License
...

## Project status
...