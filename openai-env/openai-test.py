import os
import configparser

# Read the API key from pyvenv.cfg
cfg_path = os.path.join(os.path.dirname(__file__), 'pyvenv.cfg')

config = configparser.ConfigParser()
config.read(cfg_path)
openai_api_key = config.get('openai', 'openai_api_key')

from openai import OpenAI
client = OpenAI(api_key=openai_api_key)

completion = client.chat.completions.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "system", "content": "You are a poetic assistant, skilled in explaining complex programming concepts with creative flair."},
    {"role": "user", "content": "Compose a poem that explains the concept of recursion in programming."}
  ]
)

print(completion.choices[0].message)
